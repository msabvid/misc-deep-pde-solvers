import numpy as np
import torch
import torch.nn as nn
import glob
import os
import argparse
from scipy.stats import random_correlation
from numpy.linalg import cholesky
import math


parser = argparse.ArgumentParser()
parser.add_argument('--device', action="store", type=int, default=0)
parser.add_argument('--epochs', action="store", type=int, default=100)
parser.add_argument('--N_steps', action="store", type=int, default=100, help="dimension of the PDE")
parser.add_argument('--dim', action="store", type=int, default=2, help="dimension of the PDE")
parser.add_argument('--batch_size', action="store", type=int, default=5000, help="dimension of the PDE")


args = parser.parse_args()

if torch.cuda.is_available():
    device = "cuda:"+str(args.device)
else:
    device = "cpu"

class Net_timestep(nn.Module):
    
    def __init__(self, dim, nOut, n_layers, vNetWidth, activation = "relu"):
        super(Net_timestep, self).__init__()
        self.dim = dim
        self.nOut = nOut
        
        if activation!="relu" and activation!="tanh":
            raise ValueError("unknown activation function {}".format(activation))
        if activation == "relu":
            self.activation = nn.ReLU()
        else:
            self.activation = nn.Tanh()
        
        self.i_h = self.hiddenLayerT0(dim, vNetWidth)
        self.h_h = nn.ModuleList([self.hiddenLayerT1(vNetWidth, vNetWidth) for l in range(n_layers-1)])
        self.h_o = self.outputLayer(vNetWidth, nOut)
        
    def hiddenLayerT0(self,  nIn, nOut):
        layer = nn.Sequential(nn.BatchNorm1d(nIn, momentum=0.1), nn.Linear(nIn,nOut,bias=True),
                              nn.BatchNorm1d(nOut, momentum=0.1),   
                              self.activation)   
        return layer
    
    def hiddenLayerT1(self, nIn, nOut):
        layer = nn.Sequential(nn.Linear(nIn,nOut,bias=True),
                              nn.BatchNorm1d(nOut, momentum=0.1),  
                              self.activation)   
        return layer
    
    
    def outputLayer(self, nIn, nOut):
        layer = nn.Sequential(nn.Linear(nIn, nOut,bias=True),nn.BatchNorm1d(nOut, momentum=0.1))
        return layer
    
    def forward(self, S):
        h = self.i_h(S)
        for l in range(len(self.h_h)):
            h = self.h_h[l](h)
        output = self.h_o(h)
        return output




class Path_granularity(nn.Module):
    """
    Main reference: https://arxiv.org/abs/1806.00421 
    """
    
    def __init__(self, dim, r, sigma, covariance_mat, timegrid, granularity=2, device="cpu"):
        super(Path_granularity, self).__init__()
        self.dim = dim
        self.timegrid = torch.Tensor(timegrid).to(device)
        self.r = r # r is a number
        self.sigma = torch.Tensor(sigma).to(device) # this should be a vector of length dim
        self.covariance_mat = covariance_mat # covariance matrix
        self.C = cholesky(covariance_mat) # Cholesky decomposition of covariance matrix, with size (dim,dim)
        self.device=device
        self.granularity = granularity

        self.volatility_mat = torch.Tensor(self.C).to(device)
        for i in range(self.dim):
            self.volatility_mat[i] = self.volatility_mat[i]*self.sigma[i]

    def forward(self, S0):        
        S_old = S0
        path = [S_old]
        path_fine = [S_old]
        path_volatility = []
        vol = 0
        for i in range(1,len(self.timegrid)):
            # Wiener process at time timegrid[i]
            h = self.timegrid[i]-self.timegrid[i-1]
            dW = math.sqrt(h)*torch.randn(S_old.data.size(), device=self.device)#.to(device)
            # volatility(t,S) * dW
            volatility_of_S_dW = S_old * torch.matmul(self.volatility_mat,dW.transpose(1,0)).transpose(1,0) # this is a matrix of size (batch_size x dim)
            #path_volatility.append(volatility_of_S_dW)            
            vol = vol + volatility_of_S_dW
            # we update the SDE path. Use one or the other. 
            S_new = S_old  + self.r*S_old*h + volatility_of_S_dW 
            # we are done, prepare for next round
            #increment = self.r*S_old*h + volatility_of_S_dW
            S_old = S_new
            path_fine.append(S_old)
            if i % self.granularity == 0:
                path.append(S_old)
                path_volatility.append(vol)
                vol = 0                                         
        path = torch.cat([X.unsqueeze(1) for X in path], 1)
        path_fine = torch.cat([X.unsqueeze(1) for X in path_fine], 1)
        path_volatility =torch.cat([X.unsqueeze(1) for X in path_volatility], 1)

        return path, path_fine, path_volatility


def exchange_options(S):
    zeros = torch.zeros(S.size()[0], 1, device=device)
    dim = S.size()[1]
    if dim>1:
        # terminal condition in d>1 dimensions
        m = torch.cat([zeros, (S[:,0]-1/(dim-1)*torch.sum(S[:,1:],1)).view(-1,1)],1)
    else:
        # terminal condition in d=1 dimension
        m = torch.cat([zeros, (S[:,0]).view(-1,1)],1)
    #m = torch.cat([zeros, (S[:,0]-S[:,1]).view(-1,1)],1)
    output = torch.max(m, 1)
    return output[0]


class Net(nn.Module):
    def __init__(self, dim, r, timegrid, n_layers, vNetWidth, device="cuda"):
        super(Net, self).__init__()
        self.r = r
        self.device = device
        self.timegrid = torch.tensor(timegrid, device=device)
        
        self.net_timegrid_gradient = nn.ModuleList([Net_timestep(dim=dim, nOut=dim, n_layers=n_layers, vNetWidth=vNetWidth) for t in timegrid[:-1]])
        self.net_timegrid_value = nn.ModuleList([Net_timestep(dim=dim, nOut=1, n_layers=n_layers, vNetWidth=vNetWidth) for t in timegrid[:-1]])
        
    def Margrabe_formula(self, S, sigma, t, T):
        #Sigma = torch.cat([self.sigma, self.sigma])
        # sigma should be a vector of length dim
        sigma_bar = torch.sqrt(torch.dot(sigma, sigma))
        d1 = (torch.log(S[:,0]/S[:,1]) + 0.5*sigma_bar**2 * (T-t))/(sigma_bar*math.sqrt(T-t))
        d2 = d1 - sigma_bar*math.sqrt(T-t)
        N = torch.distributions.normal.Normal(loc=0, scale=1)
        output = S[:,0]*N.cdf(d1.cpu()).to(self.device) - S[:,1]*N.cdf(d2.cpu()).to(self.device)
        return output


    def get_loss(self, path, path_volatility):
        """
        with this code I'm assuming there is no granularity
        """
        grad = [self.net_timegrid_gradient[i](path[:,i,:]) for i in range(path.shape[1]-1)]
        val = [self.net_timegrid_value[i](path[:,i,:]) for i in range(path.shape[1]-1)]
        
        error = 0
        for i in range(1,len(self.timegrid)):
            h = self.timegrid[i] - self.timegrid[i-1]
            stoch_int = torch.bmm(grad[i-1].unsqueeze(1), path_volatility[:,i-1,:].unsqueeze(2)).squeeze(1)
            #stoch_int = torch.bmm(Z[:,i-1,:].unsqueeze(1), path_volatility[:,i-1,:].unsqueeze(2)).squeeze(1)
            if i == len(self.timegrid)-1:
                #path_terminal = [path[:,j,:] for j in range(path.shape[1])]
                S_T = path[:,-1,:]
                #terminal = asian_options(path_terminal, self.timegrid, self.device).view(-1,1)
                terminal = exchange_options(S_T).view(-1,1)
                #error_from_step_i = terminal.detach() - Y[:,i-1,:]*(1+self.r*h) - stoch_int
                error_from_step_i = terminal.detach() - val[i-1]*(1+self.r*h) - stoch_int
            else:
                #error_from_step_i = (Y[:,i,:].detach() - Y[:,i-1,:]*(1+self.r*h) - stoch_int)/len(self.timegrid)
                error_from_step_i = (val[i].detach() - val[i-1]*(1+self.r*h) - stoch_int)#/len(self.timegrid)
            error += error_from_step_i**2
        return error.mean()

    def get_loss_iterative(self, path, path_volatility, timestep):
        
        grad = self.net_timegrid_gradient[timestep](path[:,timestep,:])
        val_old = self.net_timegrid_value[timestep](path[:,timestep,:])
        stoch_int = torch.bmm(grad.unsqueeze(1), path_volatility[:,timestep,:].unsqueeze(2)).squeeze(1)
        if timestep == len(self.net_timegrid_value) - 1:
            S_T = path[:,-1,:]
            val_new = exchange_options(S_T).view(-1,1)
        else:
            #self.net_timegrid_value[timestep+1].eval()
            val_new = self.net_timegrid_value[timestep+1](path[:,timestep+1,:])
        h = self.timegrid[timestep+1]-self.timegrid[timestep]
        error_from_step = val_new.detach() - val_old * (1+self.r*h) - stoch_int
        error = torch.mean(error_from_step**2)
        return error

    def get_control_variate(self, path, path_volatility):
        
        grad = [self.net_timegrid_gradient[i](path[:,i,:]) for i in range(path.shape[1]-1)]
        
        control_variate = 0
        for i in range(path_volatility.shape[1]):
            control_variate += torch.bmm(torch.exp(-self.r*self.timegrid[i])*grad[i].unsqueeze(1), path_volatility[:,i,:].unsqueeze(2)).squeeze(1)
        return control_variate



def train_BSDE_iterative():

    model.train()
    path_results = "/disk/homeDIRS/s2001981/BS_asian_adversarial/results_BSDE_fixed_sigma"
    max_timestep = len(model.net_timegrid_value)-1
    for timestep in range(max_timestep, -1,-1):
        for it in range(args.epochs):
            model.train()
            optimizer.zero_grad()
            z = torch.randn([args.batch_size, dim], device="cpu") 
            input = torch.exp((mu-0.5*sigma**2)*tau + sigma*math.sqrt(tau)*z)
            with torch.no_grad():
                path, path_fine, path_volatility = path_gen(input)
            path = path.to(device)
            path_volatility = path_volatility.to(device)
            path_fine = path_fine.to(device)

            #S_old, control_variate, path_output = model(path, path_volatility)
            loss = model.get_loss_iterative(path, path_volatility, timestep)
            loss.backward()

            optimizer.step()

            with open(os.path.join(path_results, "log_path_prepare_train.txt"), "a") as f:
                f.write("timestep: {}, iteration: {}, loss: {:.8f}\n".format(timestep, it, loss))
    
        test(timestep, model)


def train_BSDE():
    model.train()
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5000, gamma=0.1)
    path_results = "/disk/homeDIRS/s2001981/BS_asian_adversarial/results_BSDE_fixed_sigma"
    

    for it in range(args.epochs):
        model.train()
        optimizer.zero_grad()
        z = torch.randn([args.batch_size, dim], device="cpu") # we prepare data on cpu
        input = torch.exp((mu-0.5*sigma**2)*tau + sigma*math.sqrt(tau)*z)
        #with torch.no_grad():
        #    path, path_signature, path_volatility = path_gen(input)
        with torch.no_grad():
            path, path_fine, path_volatility = path_gen(input)
        
        path = path.to(device)
        path_volatility = path_volatility.to(device)
        path_fine = path_fine.to(device)

        #S_old, control_variate, path_output = model(path, path_volatility)
        loss = model.get_loss(path, path_volatility)
        loss.backward()

        optimizer.step()
        scheduler.step()

        with open(os.path.join(path_results, "log_path_prepare_train.txt"), "a") as f:
            f.write("iteration: {}, loss: {:.8f}\n".format(it, loss))
            
        if (it+1) % 100 == 0:
            test(it, model)
            state = {"it":it+1, "state_dict":model.state_dict(), "optimizer":optimizer.state_dict()}
            filename = "model_epoch"+str(it+1)+"_N_steps"+str(args.N_steps)+".pth.tar"
            torch.save(state, os.path.join(path_results, filename))



def get_error(model):
    model.eval()
    if args.dim != 2:
        raise ValueError("Margrabe formula only applies to 2-dimensional Exchange Problem!")
    S = torch.ones(1, args.dim, device=device)
    sigma = 0.3
    sigma = torch.tensor([sigma, sigma]).to(device)
    with torch.no_grad():
        solution_Margrabe = model.Margrabe_formula(S=S, sigma=sigma, t=0, T=0.5)
        prediction = model.net_timegrid_value[0](S)
    return solution_Margrabe.item(),prediction.item() 



def test(it, model):
    path_results = "/disk/homeDIRS/s2001981/BS_asian_adversarial/results_BSDE_fixed_sigma"
    torch.manual_seed(0)
    model.eval()
    input = torch.ones(20000, args.dim)
    #with torch.no_grad():
    #    path, path_signature, path_volatility = path_gen(input)
    with torch.no_grad():
        path, path_fine, path_volatility = path_gen(input)

    path = path.to(device)
    path_volatility = path_volatility.to(device)
    path_fine = path_fine.to(device)

    with torch.no_grad():
        #S_old, control_variate, path_output = model(path, path_volatility)
        control_variate = model.get_control_variate(path, path_volatility)

    path_fine = [path_fine[:,i,:] for i in range(path_fine.shape[1])]
    S_T = path_fine[-1]#[:,-1,:]
    terminal = torch.exp(torch.tensor([-T*r], device=device))*exchange_options(S_T).view(-1,1)

    cov_terminal_control_variate = torch.mean((terminal-torch.mean(terminal))*(control_variate-torch.mean(control_variate)))
    var_control_variate = torch.var(control_variate)
    var_terminal = torch.var(terminal)
    corr_terminal_control_variate = cov_terminal_control_variate/(torch.sqrt(var_control_variate)*torch.sqrt(var_terminal))
    b = cov_terminal_control_variate / var_control_variate

    MC_CV = terminal - b*(control_variate)

    MC_CV_estimator = torch.mean(MC_CV)
    var_MC_CV = torch.var(MC_CV)
    var_reduction_factor = 1-corr_terminal_control_variate.item()**2
    
    solution_Margrabe, prediction = get_error(model)
    with open(os.path.join(path_results, "test_prepare_train_N_steps"+str(args.N_steps)+".txt"),"a") as f:
        f.write("{},{:.5f},{:.5f},{:.5f},{:.5f},{:.5f},{:.5f},{:.5f}\n".format(it+1, terminal.mean().item(), MC_CV.mean().item(), terminal.var().item(), MC_CV.var().item(), corr_terminal_control_variate.item(), solution_Margrabe, prediction))


if __name__ == "__main__":
    # we fix the seeds for reproducibility
    torch.manual_seed(0)
    np.random.seed(0)

    # we set up the parameters of the problem
    init_t, T = 0,0.5
    N_steps = args.N_steps#100
    timestep = T/N_steps
    timegrid = np.linspace(init_t, T, N_steps+1)
    dim = args.dim#2#dim = 2
    r = 0.05
    sigma = 0.3 
    mu = 0.08
    tau = 0.1

    # we generate a covariance matrix to have correlated brownian motions
    eig = np.random.uniform(0,1,dim)
    eig = dim*eig/eig.sum()
    #covariance_mat = random_correlation.rvs(eig)
    covariance_mat = np.identity(dim)  
    
    # we create path generator
    path_gen = Path_granularity(dim=dim, r=r, sigma=np.array([sigma]*dim), covariance_mat=covariance_mat, timegrid=timegrid, granularity=1, device="cpu")
    
    model = Net(dim=dim, r=r, timegrid=timegrid, vNetWidth=20, n_layers=2, device=device)
    #model = Net_granularity(dim, granularity = 2, timegrid=timegrid, device=device)
    model.to(device)
    
    batch_size = 5000
    base_lr = 0.001
    optimizer = torch.optim.Adam(model.parameters(), lr = base_lr)

    path_results = "/disk/homeDIRS/s2001981/BS_asian_adversarial/results_BSDE_fixed_sigma"
    with open(os.path.join(path_results, "test_prepare_train_N_steps"+str(args.N_steps)+".txt"),"w") as f:
        f.write("it,MC,MC_CV,MC_var,MC_CV_var,cori,Margrabe,prediction\n")
    train_BSDE()               
    #train_BSDE_iterative()


