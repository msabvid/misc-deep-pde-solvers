# Misc-Deep-PDE-Solvers

Compilation of PDE solvers using Deep Learning with no specific order or 
consistency between scripts / name of scripts.

- `DL_Heat_Equation.ipynb`: Solver of heat equation using [this](https://arxiv.org/pdf/1708.07469.pdf) network architecture.
- `BS_exchange_fixed_sigma_BSDE.py`: Solver of Black Scholes model with exchange options, using the ideas from [here](https://arxiv.org/abs/1706.04702), extended [here](https://arxiv.org/abs/1810.05094). To run this code, change the paths for logging, and run on the terminal:
```python
python BS_exchange_fixed_sigma_BSDE.py --device 0 --epochs 10000 --N_steps 50 --dim 2 --batch_size 1000
```    
